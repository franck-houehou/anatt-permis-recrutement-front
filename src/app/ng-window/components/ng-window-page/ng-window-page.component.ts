import { Component } from '@angular/core';

@Component({
  selector: 'app-ng-window-page',
  templateUrl: './ng-window-page.component.html',
  styleUrls: ['./ng-window-page.component.scss'],
})
export class NgWindowPageComponent {}
