export type RespType = {
  data?: any;
  success: boolean;
  errors?: any;
  message: string;
};
