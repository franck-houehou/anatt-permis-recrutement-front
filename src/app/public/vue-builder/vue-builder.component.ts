import { Component } from '@angular/core';
import { isFile } from 'src/app/helpers/helpers';

@Component({
  selector: 'app-vue-builder',
  templateUrl: './vue-builder.component.html',
  styleUrls: ['./vue-builder.component.scss'],
})
export class VueBuilderComponent {}
