export interface Prestation {
  title: string;
  image: string;
  slug: string;
}
