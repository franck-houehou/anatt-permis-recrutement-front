export interface Titre {
  id?: number;
  name?: string;
  status: boolean;
}
