export interface ActeSigne {
  id?: number;
  name: string;
  status: boolean;
  is_one_signataire: boolean;
  signataires: any;
}
