export interface SessionEntreprise {
  id?: number;
  user_id?: number;
  categorie_permis_id?: number;
  annexe_id?: number;
  date_compo: string;
  date_soumission: string;
  state: any;
}
