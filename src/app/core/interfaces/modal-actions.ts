export type ModalActions = 'store' | 'edit' | 'show' | string;
