export interface TrancheAge {
  id?: number;
  age_min: number | null;
  age_max: number | null;
  status?: boolean | null;
  validite?: number | null;
}
