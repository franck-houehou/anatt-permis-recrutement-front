export interface Agregateur {
  id?: number;
  name?: string;
  status: boolean;
  photo: any;
}
