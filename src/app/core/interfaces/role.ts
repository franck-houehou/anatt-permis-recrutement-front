export interface Role {
  id?: number;
  name?: string;
  nom_complet: string;
  permissions: any;
}
