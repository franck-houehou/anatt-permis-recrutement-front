export interface CandidatEntreprise {
  id?: number;
  npi: string;
  langue_id: string;
  recrutement_id: string;
  num_permis: string;
  permis_file: string;
}
