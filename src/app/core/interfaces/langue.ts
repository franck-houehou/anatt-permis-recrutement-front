export interface Langue {
  id?: number;
  name?: string;
  status: boolean;
}
