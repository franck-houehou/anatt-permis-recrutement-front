export interface TypePiece {
  id?: number;
  name?: string;
  sigle?: string;
  status: boolean;
}
