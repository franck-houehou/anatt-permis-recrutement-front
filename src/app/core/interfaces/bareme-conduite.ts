export interface BaremeConduite {
  id?: number;
  categorie_permis_id?: number;
  name: string;
  poids?: number;
}
