export type ApiEndpoint =
  | 'admin'
  | 'base'
  | 'auto-ecole'
  | 'candidat'
  | 'recrutement-examinateur'
  | 'recrutement-moniteur'
  | 'entreprise'
  | 'candidatanip';
