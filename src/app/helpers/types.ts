export type KeyValueParam = {
  key: string;
  value: any;
};
