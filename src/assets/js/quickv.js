(function (M, y) {
  typeof exports == "object" && typeof module != "undefined"
    ? y(exports)
    : typeof define == "function" && define.amd
    ? define(["exports"], y)
    : ((M = typeof globalThis != "undefined" ? globalThis : M || self),
      y((M.quickv = {})));
})(this, function (M) {
  var Ee, Me, $e;
  ("use strict");
  var bt = Object.defineProperty;
  var Ce = Object.getOwnPropertySymbols;
  var wt = Object.prototype.hasOwnProperty,
    yt = Object.prototype.propertyIsEnumerable;
  var Le = (M, y, p) =>
      y in M
        ? bt(M, y, { enumerable: !0, configurable: !0, writable: !0, value: p })
        : (M[y] = p),
    L = (M, y) => {
      for (var p in y || (y = {})) wt.call(y, p) && Le(M, p, y[p]);
      if (Ce) for (var p of Ce(y)) yt.call(y, p) && Le(M, p, y[p]);
      return M;
    };
  const y = (t) => {
      const e = /^(\w+):(.+)$/,
        s = t.match(e);
      if (s) {
        const i = s[1];
        let r = s[2];
        return { ruleName: i, params: r };
      } else {
        const [i, r] = t.split(":");
        return { ruleName: i, params: r };
      }
    },
    p = (t, e = ",") =>
      typeof t != "string" ? [] : t.split(e).map((s) => s.trim()),
    F = (t) => {
      throw new Error(`Please provide <<${t}>> rule arguments`);
    };
  function fe() {
    return new Date().toISOString();
  }
  class X {
    constructor(e) {
      (this.argument = e), (this._pipe = "&pip;"), (this._space = "&esp;");
    }
    replacePipes() {
      return this.argument.replace(new RegExp(this._pipe, "g"), "|");
    }
    replaceSpaces() {
      return this.argument.replace(new RegExp(this._space, "g"), " ");
    }
  }
  var Ae =
    typeof globalThis != "undefined"
      ? globalThis
      : typeof window != "undefined"
      ? window
      : typeof global != "undefined"
      ? global
      : typeof self != "undefined"
      ? self
      : {};
  function Oe(t) {
    return t &&
      t.__esModule &&
      Object.prototype.hasOwnProperty.call(t, "default")
      ? t.default
      : t;
  }
  var de = { exports: {} };
  (function (t, e) {
    (function (s, i) {
      t.exports = i();
    })(Ae, function () {
      var s = 1e3,
        i = 6e4,
        r = 36e5,
        o = "millisecond",
        d = "second",
        m = "minute",
        R = "hour",
        A = "day",
        Y = "week",
        O = "month",
        Te = "quarter",
        S = "year",
        Q = "date",
        qe = "Invalid Date",
        gt =
          /^(\d{4})[-/]?(\d{1,2})?[-/]?(\d{0,2})[Tt\s]*(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?[.:]?(\d+)?$/,
        vt =
          /\[([^\]]+)]|Y{1,4}|M{1,4}|D{1,2}|d{1,4}|H{1,2}|h{1,2}|a|A|m{1,2}|s{1,2}|Z{1,2}|SSS/g,
        pt = {
          name: "en",
          weekdays:
            "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split(
              "_"
            ),
          months:
            "January_February_March_April_May_June_July_August_September_October_November_December".split(
              "_"
            ),
          ordinal: function (u) {
            var l = ["th", "st", "nd", "rd"],
              n = u % 100;
            return "[" + u + (l[(n - 20) % 10] || l[n] || l[0]) + "]";
          },
        },
        oe = function (u, l, n) {
          var h = String(u);
          return !h || h.length >= l
            ? u
            : "" + Array(l + 1 - h.length).join(n) + u;
        },
        _t = {
          s: oe,
          z: function (u) {
            var l = -u.utcOffset(),
              n = Math.abs(l),
              h = Math.floor(n / 60),
              a = n % 60;
            return (l <= 0 ? "+" : "-") + oe(h, 2, "0") + ":" + oe(a, 2, "0");
          },
          m: function u(l, n) {
            if (l.date() < n.date()) return -u(n, l);
            var h = 12 * (n.year() - l.year()) + (n.month() - l.month()),
              a = l.clone().add(h, O),
              c = n - a < 0,
              f = l.clone().add(h + (c ? -1 : 1), O);
            return +(-(h + (n - a) / (c ? a - f : f - a)) || 0);
          },
          a: function (u) {
            return u < 0 ? Math.ceil(u) || 0 : Math.floor(u);
          },
          p: function (u) {
            return (
              { M: O, y: S, w: Y, d: A, D: Q, h: R, m, s: d, ms: o, Q: Te }[
                u
              ] ||
              String(u || "")
                .toLowerCase()
                .replace(/s$/, "")
            );
          },
          u: function (u) {
            return u === void 0;
          },
        },
        j = "en",
        W = {};
      W[j] = pt;
      var ue = function (u) {
          return u instanceof K;
        },
        Z = function u(l, n, h) {
          var a;
          if (!l) return j;
          if (typeof l == "string") {
            var c = l.toLowerCase();
            W[c] && (a = c), n && ((W[c] = n), (a = c));
            var f = l.split("-");
            if (!a && f.length > 1) return u(f[0]);
          } else {
            var v = l.name;
            (W[v] = l), (a = v);
          }
          return !h && a && (j = a), a || (!h && j);
        },
        E = function (u, l) {
          if (ue(u)) return u.clone();
          var n = typeof l == "object" ? l : {};
          return (n.date = u), (n.args = arguments), new K(n);
        },
        g = _t;
      (g.l = Z),
        (g.i = ue),
        (g.w = function (u, l) {
          return E(u, { locale: l.$L, utc: l.$u, x: l.$x, $offset: l.$offset });
        });
      var K = (function () {
          function u(n) {
            (this.$L = Z(n.locale, null, !0)), this.parse(n);
          }
          var l = u.prototype;
          return (
            (l.parse = function (n) {
              (this.$d = (function (h) {
                var a = h.date,
                  c = h.utc;
                if (a === null) return new Date(NaN);
                if (g.u(a)) return new Date();
                if (a instanceof Date) return new Date(a);
                if (typeof a == "string" && !/Z$/i.test(a)) {
                  var f = a.match(gt);
                  if (f) {
                    var v = f[2] - 1 || 0,
                      b = (f[7] || "0").substring(0, 3);
                    return c
                      ? new Date(
                          Date.UTC(
                            f[1],
                            v,
                            f[3] || 1,
                            f[4] || 0,
                            f[5] || 0,
                            f[6] || 0,
                            b
                          )
                        )
                      : new Date(
                          f[1],
                          v,
                          f[3] || 1,
                          f[4] || 0,
                          f[5] || 0,
                          f[6] || 0,
                          b
                        );
                  }
                }
                return new Date(a);
              })(n)),
                (this.$x = n.x || {}),
                this.init();
            }),
            (l.init = function () {
              var n = this.$d;
              (this.$y = n.getFullYear()),
                (this.$M = n.getMonth()),
                (this.$D = n.getDate()),
                (this.$W = n.getDay()),
                (this.$H = n.getHours()),
                (this.$m = n.getMinutes()),
                (this.$s = n.getSeconds()),
                (this.$ms = n.getMilliseconds());
            }),
            (l.$utils = function () {
              return g;
            }),
            (l.isValid = function () {
              return this.$d.toString() !== qe;
            }),
            (l.isSame = function (n, h) {
              var a = E(n);
              return this.startOf(h) <= a && a <= this.endOf(h);
            }),
            (l.isAfter = function (n, h) {
              return E(n) < this.startOf(h);
            }),
            (l.isBefore = function (n, h) {
              return this.endOf(h) < E(n);
            }),
            (l.$g = function (n, h, a) {
              return g.u(n) ? this[h] : this.set(a, n);
            }),
            (l.unix = function () {
              return Math.floor(this.valueOf() / 1e3);
            }),
            (l.valueOf = function () {
              return this.$d.getTime();
            }),
            (l.startOf = function (n, h) {
              var a = this,
                c = !!g.u(h) || h,
                f = g.p(n),
                v = function (k, N) {
                  var x = g.w(
                    a.$u ? Date.UTC(a.$y, N, k) : new Date(a.$y, N, k),
                    a
                  );
                  return c ? x : x.endOf(A);
                },
                b = function (k, N) {
                  return g.w(
                    a
                      .toDate()
                      [k].apply(
                        a.toDate("s"),
                        (c ? [0, 0, 0, 0] : [23, 59, 59, 999]).slice(N)
                      ),
                    a
                  );
                },
                _ = this.$W,
                q = this.$M,
                I = this.$D,
                D = "set" + (this.$u ? "UTC" : "");
              switch (f) {
                case S:
                  return c ? v(1, 0) : v(31, 11);
                case O:
                  return c ? v(1, q) : v(0, q + 1);
                case Y:
                  var z = this.$locale().weekStart || 0,
                    U = (_ < z ? _ + 7 : _) - z;
                  return v(c ? I - U : I + (6 - U), q);
                case A:
                case Q:
                  return b(D + "Hours", 0);
                case R:
                  return b(D + "Minutes", 1);
                case m:
                  return b(D + "Seconds", 2);
                case d:
                  return b(D + "Milliseconds", 3);
                default:
                  return this.clone();
              }
            }),
            (l.endOf = function (n) {
              return this.startOf(n, !1);
            }),
            (l.$set = function (n, h) {
              var a,
                c = g.p(n),
                f = "set" + (this.$u ? "UTC" : ""),
                v = ((a = {}),
                (a[A] = f + "Date"),
                (a[Q] = f + "Date"),
                (a[O] = f + "Month"),
                (a[S] = f + "FullYear"),
                (a[R] = f + "Hours"),
                (a[m] = f + "Minutes"),
                (a[d] = f + "Seconds"),
                (a[o] = f + "Milliseconds"),
                a)[c],
                b = c === A ? this.$D + (h - this.$W) : h;
              if (c === O || c === S) {
                var _ = this.clone().set(Q, 1);
                _.$d[v](b),
                  _.init(),
                  (this.$d = _.set(Q, Math.min(this.$D, _.daysInMonth())).$d);
              } else v && this.$d[v](b);
              return this.init(), this;
            }),
            (l.set = function (n, h) {
              return this.clone().$set(n, h);
            }),
            (l.get = function (n) {
              return this[g.p(n)]();
            }),
            (l.add = function (n, h) {
              var a,
                c = this;
              n = Number(n);
              var f = g.p(h),
                v = function (q) {
                  var I = E(c);
                  return g.w(I.date(I.date() + Math.round(q * n)), c);
                };
              if (f === O) return this.set(O, this.$M + n);
              if (f === S) return this.set(S, this.$y + n);
              if (f === A) return v(1);
              if (f === Y) return v(7);
              var b = ((a = {}), (a[m] = i), (a[R] = r), (a[d] = s), a)[f] || 1,
                _ = this.$d.getTime() + n * b;
              return g.w(_, this);
            }),
            (l.subtract = function (n, h) {
              return this.add(-1 * n, h);
            }),
            (l.format = function (n) {
              var h = this,
                a = this.$locale();
              if (!this.isValid()) return a.invalidDate || qe;
              var c = n || "YYYY-MM-DDTHH:mm:ssZ",
                f = g.z(this),
                v = this.$H,
                b = this.$m,
                _ = this.$M,
                q = a.weekdays,
                I = a.months,
                D = function (N, x, he, J) {
                  return (N && (N[x] || N(h, c))) || he[x].slice(0, J);
                },
                z = function (N) {
                  return g.s(v % 12 || 12, N, "0");
                },
                U =
                  a.meridiem ||
                  function (N, x, he) {
                    var J = N < 12 ? "AM" : "PM";
                    return he ? J.toLowerCase() : J;
                  },
                k = {
                  YY: String(this.$y).slice(-2),
                  YYYY: g.s(this.$y, 4, "0"),
                  M: _ + 1,
                  MM: g.s(_ + 1, 2, "0"),
                  MMM: D(a.monthsShort, _, I, 3),
                  MMMM: D(I, _),
                  D: this.$D,
                  DD: g.s(this.$D, 2, "0"),
                  d: String(this.$W),
                  dd: D(a.weekdaysMin, this.$W, q, 2),
                  ddd: D(a.weekdaysShort, this.$W, q, 3),
                  dddd: q[this.$W],
                  H: String(v),
                  HH: g.s(v, 2, "0"),
                  h: z(1),
                  hh: z(2),
                  a: U(v, b, !0),
                  A: U(v, b, !1),
                  m: String(b),
                  mm: g.s(b, 2, "0"),
                  s: String(this.$s),
                  ss: g.s(this.$s, 2, "0"),
                  SSS: g.s(this.$ms, 3, "0"),
                  Z: f,
                };
              return c.replace(vt, function (N, x) {
                return x || k[N] || f.replace(":", "");
              });
            }),
            (l.utcOffset = function () {
              return 15 * -Math.round(this.$d.getTimezoneOffset() / 15);
            }),
            (l.diff = function (n, h, a) {
              var c,
                f = g.p(h),
                v = E(n),
                b = (v.utcOffset() - this.utcOffset()) * i,
                _ = this - v,
                q = g.m(this, v);
              return (
                (q =
                  ((c = {}),
                  (c[S] = q / 12),
                  (c[O] = q),
                  (c[Te] = q / 3),
                  (c[Y] = (_ - b) / 6048e5),
                  (c[A] = (_ - b) / 864e5),
                  (c[R] = _ / r),
                  (c[m] = _ / i),
                  (c[d] = _ / s),
                  c)[f] || _),
                a ? q : g.a(q)
              );
            }),
            (l.daysInMonth = function () {
              return this.endOf(O).$D;
            }),
            (l.$locale = function () {
              return W[this.$L];
            }),
            (l.locale = function (n, h) {
              if (!n) return this.$L;
              var a = this.clone(),
                c = Z(n, h, !0);
              return c && (a.$L = c), a;
            }),
            (l.clone = function () {
              return g.w(this.$d, this);
            }),
            (l.toDate = function () {
              return new Date(this.valueOf());
            }),
            (l.toJSON = function () {
              return this.isValid() ? this.toISOString() : null;
            }),
            (l.toISOString = function () {
              return this.$d.toISOString();
            }),
            (l.toString = function () {
              return this.$d.toUTCString();
            }),
            u
          );
        })(),
        Ne = K.prototype;
      return (
        (E.prototype = Ne),
        [
          ["$ms", o],
          ["$s", d],
          ["$m", m],
          ["$H", R],
          ["$W", A],
          ["$M", O],
          ["$y", S],
          ["$D", Q],
        ].forEach(function (u) {
          Ne[u[1]] = function (l) {
            return this.$g(l, u[0], u[1]);
          };
        }),
        (E.extend = function (u, l) {
          return u.$i || (u(l, K, E), (u.$i = !0)), E;
        }),
        (E.locale = Z),
        (E.isDayjs = ue),
        (E.unix = function (u) {
          return E(1e3 * u);
        }),
        (E.en = W[j]),
        (E.Ls = W),
        (E.p = {}),
        E
      );
    });
  })(de);
  var Re = de.exports;
  const ee = Oe(Re),
    ce = (t) => ee(t, void 0, !0).isValid(),
    me = (t, e) => (e === "now" && (e = fe()), ee(t).isBefore(e)),
    ge = (t, e) => (e === "now" && (e = fe()), ee(t).isAfter(e)),
    De = (t, e) => {
      e || F("dateBetween");
      const [s, i] = p(e != null ? e : "");
      return ge(t, s) && me(t, i);
    },
    Be = (t) => {
      if (t.split(":").length < 3) for (; t.split(":").length < 3; ) t += ":00";
      return /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/.test(t);
    },
    G = (t, ...e) => !!t && t !== "",
    Se = (t) => !0,
    Ie = (t, e) => (e || F("in"), p(e).includes(t)),
    xe = (t, e) => (B(t) ? H(t, e) : ie(t, e)),
    Fe = (t) =>
      typeof t == "boolean" ||
      (typeof t == "string" &&
        ((t = t.toLowerCase()),
        t === "true" ||
          t === "false" ||
          t === "0" ||
          t === "1" ||
          t === "yes" ||
          t === "no")),
    Ve = (t, e) => {
      var [s, i] = p(e != null ? e : "");
      return B(t)
        ? H(t, i) && re(t, s)
        : !$(t) && !B(t)
        ? ce(t)
          ? De(t, e)
          : ve(t, e)
        : ((s = Number(s)),
          (i = Number(i)),
          t !== void 0 && t !== Number && t !== "" && $(s) && $(i)
            ? ((t = Number(t)), $(t) ? _e(t, i) && pe(t, s) : !1)
            : !1);
    },
    Pe = (t, e) => {
      if (!e) throw new Error("The regex rule argument must not be empty");
      const s = new X(e);
      return new RegExp(s.replacePipes()).test(t);
    },
    We = (t, e) =>
      e === "string"
        ? !C(t) || t.length === 0
          ? !1
          : !/\d/.test(t)
        : e === "number"
        ? $(t)
        : !1,
    Qe = (t, ...e) =>
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        t
      ),
    te = (t, e) => (t && C(t) ? t.length >= Number(e) : !1),
    se = (t, e) => (t ? (C(t) ? t.length <= Number(e) : !1) : !0),
    C = (t) => typeof t == "string",
    ke = (t) => /^(ftp|http|https):\/\/[^ "]+$/.test(t),
    je = (t) =>
      typeof t != "string" || t.length === 0 ? !1 : /^[A-Z]/.test(t),
    ze = (t) =>
      typeof t != "string" || t.length === 0 ? !1 : /^[a-z]/.test(t),
    Ue = (t, e) => {
      e || F("startWith");
      const s = p(e != null ? e : "");
      return C(t) ? s.some((i) => t.startsWith(i)) : !1;
    },
    Ge = (t, e) => {
      e || F("endWith");
      const s = p(e != null ? e : "");
      return C(t) ? s.some((i) => t.endsWith(i)) : !1;
    },
    He = (t, e) => {
      e || F("contains");
      const s = p(e != null ? e : "");
      return C(t) ? s.every((i) => t.includes(new X(i).replaceSpaces())) : !1;
    },
    ie = (t, e) => {
      if (!$(e)) throw new Error("The length rule argument must be an integer");
      return (
        (e = parseInt(e)),
        typeof t == "string" || typeof t == "number"
          ? (t = t.toString().split(""))
          : typeof t == "object" && t !== null && t !== void 0 && (t = []),
        Array.isArray(t) ? t.length === e : !1
      );
    },
    Ye = (t) => {
      const s = /[A-Z]/.test(t),
        i = /[a-z]/.test(t),
        r = /\d/.test(t),
        o = /[!@#$%^&*(),.?":{}|<>]/.test(t);
      return !(t.length < 8 || !s || !i || !r || !o);
    },
    Ze = (t) => (!C(t) || t.length === 0 ? !1 : /^[a-zA-Z]/.test(t)),
    Ke = (t) => (!C(t) || t.length === 0 ? !1 : /[a-zA-Z]$/.test(t)),
    Je = (t) => (typeof t != "string" ? !1 : /[a-zA-Z]/.test(t)),
    Xe = (t, e) => {
      const s = p(e);
      return (
        s.length || F("excludes"),
        C(t) ? !s.some((i) => t.includes(new X(i).replaceSpaces())) : !0
      );
    },
    et = (t, e) => t === t.toLocaleUpperCase(),
    tt = (t, e) => t === t.toLocaleLowerCase(),
    ve = (t, e) => {
      const [s, i] = p(e != null ? e : "");
      return te(t, s) && se(t, i);
    },
    B = (t) => t instanceof File || t instanceof Blob,
    H = (t, e) => {
      if (B(t)) {
        const i = t.size;
        let r;
        const o =
          e == null ? void 0 : e.match(/^(\d+(\.\d+)?)\s*(B|KB|MB|GB)$/i);
        if (!o)
          throw new Error(
            "Invalid maxSize format. Please use valid format like '1KB', '1MB', etc."
          );
        const d = parseFloat(o[1]),
          m = o[3].toUpperCase();
        return (
          m === "KB"
            ? (r = d * 1024)
            : m === "MB"
            ? (r = d * 1024 * 1024)
            : m === "GB"
            ? (r = d * 1024 * 1024 * 1024)
            : (r = d),
          i <= r
        );
      } else return !1;
    },
    re = (t, e) => {
      if (B(t)) {
        const i = t.size;
        let r;
        const o =
          e == null ? void 0 : e.match(/^(\d+(\.\d+)?)\s*(B|KB|MB|GB)$/i);
        if (!o)
          throw new Error(
            "Invalid minSize format. Please use valid format like '1KB', '1MB', etc."
          );
        const d = parseFloat(o[1]),
          m = o[3].toUpperCase();
        return (
          m === "KB"
            ? (r = d * 1024)
            : m === "MB"
            ? (r = d * 1024 * 1024)
            : m === "GB"
            ? (r = d * 1024 * 1024 * 1024)
            : (r = d),
          i >= r
        );
      } else return !1;
    },
    st = (t, e) => {
      var s;
      if ((e || F("mimes"), t instanceof File)) {
        const i = t;
        return (
          (s = e == null ? void 0 : e.split(",").map((o) => o.trim())) != null
            ? s
            : []
        ).some((o) => {
          if (((o = o.replace(/\s/g, "")), o === "*")) return !0;
          if (o.endsWith("/*")) {
            const d = o.slice(0, -2);
            return i.type.startsWith(d);
          } else if (o.startsWith("*.")) {
            const d = o.substring(2);
            return i.name.endsWith(d);
          } else return i.type === o;
        });
      } else return !1;
    },
    pe = (t, e) => {
      if (B(t)) return re(t, e);
      if ((t == null && (t = 0), !$(e)))
        throw new Error("Min rule parameter must be an integer");
      return $(t) ? Number(t) >= Number(e) : te(t, e);
    },
    _e = (t, e) => {
      if (B(t)) return H(t, e);
      if (!$(e)) throw new Error("Min rule parameter must be an integer");
      return t == null && (t = 0), $(t) ? Number(t) <= Number(e) : se(t, e);
    },
    be = (t) => ($(t) ? Number.isInteger(Number(t)) : !1),
    $ = (t) =>
      t === "" || t === null
        ? !1
        : t === "0" || t === 0 || t === "1" || t === 1
        ? !0
        : !isNaN(Number(t)) && typeof t != "boolean" && typeof t != "object",
    we = (t, e) => {
      if (!$(e)) throw new Error("Modulo rule parameter must be an integer");
      return $(t) ? Number(t) % Number(e) === 0 : !1;
    },
    it = {
      default: "This field is invalid",
      required: "This field is required",
      email: "Please enter a valid email address",
      maxlength: "The maximum number of allowed characters is: :arg0",
      minlength: "The minimum number of allowed characters is: :arg0",
      min: "The :field field must be greater than or equal to ':arg0'",
      max: "The :field field must be less than or equal to ':arg0'",
      string: "Please enter a string of characters",
      between: "The value of this field must be between ':arg0' and ':arg1'",
      startWith: "The :field field must start with ':arg0'",
      endWith: "The :field field must end with ':arg0'",
      contains: "The :field field must contain the value ':arg0'",
      in: "Please choose a correct value for the :field field",
      integer: "The :field field must be an integer",
      int: "The :field field must be an integer",
      number: "This field must be a number",
      numeric: "This field must be a number",
      file: "This field must be a file",
      url: "This field must be a valid URL",
      length: "The length of this field must be :arg0",
      len: "The length of this field must be :arg0",
      maxFileSize: "The file size must be less than :arg0.",
      minFileSize: "The file size must be greater than :arg0.",
      size: "The size of this field must be less than or equal to :arg0",
      boolean: "This field must be a boolean (yes or no)",
      startWithUpper: "This field must start with an uppercase letter",
      startWithLower: "This field must start with a lowercase letter",
      nullable: "",
      password:
        "The password must be at least 8 characters long and include an uppercase letter, a lowercase letter, a digit, and a special character.",
      date: "This field must be a valid date",
      before: "The date must be before (:arg0)",
      after: "The date must be after (:arg0)",
      same: "This field must be identical to the value of the :arg0 field",
      requiredIf:
        "The :field field is required when the :arg0 field has the current value",
      requiredWhen:
        "The :field field is required when the :arg0 fields are present",
      phone: "This phone number seems to be invalid",
      time: "The :field field must be a valid time.",
      startWithLetter: "The :field field must start with a letter",
      endWithLetter: "The :field field must end with a letter",
      excludes: "The :field field must not contain :arg0.",
      hasLetter: "This field must contain at least one letter",
      regex: "This field is invalid.",
      lower: "This field must be lowercase",
      upper: "This field must be uppercase",
      fileBetween: "File size must be between :arg0 and :arg1",
      stringBetween:
        "The number of characters allowed must be between :arg0 and :arg1",
      modulo: "The value of :field field must be a multiple of :arg0",
      mod: "The value of :field field must be a multiple of :arg0",
      only: "The format of this field is not accepted, as it contains non-allowed characters",
      mimes: "This file format is not supported",
    },
    w = class {
      static getMessages(t) {
        t = t != null ? t : w.DEFAULT_LANG;
        let e = w._message[t];
        return e || (e = w._message[w.DEFAULT_LANG]), e;
      }
      static getRuleMessage(t, e) {
        var i;
        const s = w.getMessages(e);
        return (i = s[t]) != null ? i : s.default;
      }
      static addMessage(t, e, s) {
        if (e) {
          const i = w.getMessages(s);
          (i[t] = e), w.putMessages(i, s);
        }
      }
      static putMessages(t, e) {
        if (!t || Object.keys(t).length === 0)
          throw new Error("The 'messages' argument must be a non-empty object");
        e = e || w.DEFAULT_LANG;
        const s = w._message[e] || {},
          i = L(L({}, s), t);
        w._message[e] = i;
      }
      static translate(t, e) {
        if (typeof t != "string" || !t.length)
          throw new Error(
            "The first argument must be a string with one or more characters"
          );
        if (typeof e != "object" || typeof e === void 0)
          throw new Error(
            "The second argument must be a valid key/value pair object"
          );
        w._message[t] = L(L({}, w.getMessages(t)), e);
      }
      static rewrite(t, e, s) {
        w.addMessage(e, s, t);
      }
      static rewriteMany(t, e, s) {
        if (typeof t != "string" || !t.length)
          throw new Error(
            "The 'lang' argument must be a string with one or more characters"
          );
        if (!Array.isArray(e) || !Array.isArray(s))
          throw new Error(
            "The 'rules' and 'messages' arguments must be arrays"
          );
        if (e.length !== s.length)
          throw new Error(
            "The 'rules' and 'messages' arrays must have the same length"
          );
        for (let i = 0; i < e.length; i++) {
          const r = e[i],
            o = s[i];
          w.rewrite(t, r, o);
        }
      }
      static local(t) {
        if (!C(t) || !t.length)
          throw new Error("The language must be a valid string");
        w._useLang = t;
      }
      static getLocal() {
        var t;
        return (t = w._useLang) != null ? t : w.LANG;
      }
    };
  let T = w;
  (T._useLang = null),
    (T.DEFAULT_LANG = "en"),
    (T.LANG = w.DEFAULT_LANG),
    (T._message = { en: it });
  const ne = {
    invalidClass: "is-invalid",
    validClass: "",
    local: { lang: T.DEFAULT_LANG },
  };
  class rt {
    constructor(e) {
      this.messages = T.getMessages(e != null ? e : T.getLocal());
    }
    getRulesMessages(e) {
      const s = [];
      for (const i of e)
        this.messages[i]
          ? s.push(this.messages[i])
          : s.push("The input value is not valid");
      return s;
    }
    parseMessage(e, s, i, r) {
      const o = this._createParamObject(p(r != null ? r : ""));
      return (o.field = e), (i = this._replace(i, o)), i;
    }
    setMessages(e) {
      return (this.messages = L(L({}, this.messages), e)), this;
    }
    _replace(e, s) {
      for (const i in s)
        if (Object.prototype.hasOwnProperty.call(s, i)) {
          const r = s[i];
          e = e.replace(`:${i}`, r);
        }
      return (
        delete s.field, e.replace(/\.\.\.arg/, Object.values(s).join(", "))
      );
    }
    _createParamObject(e) {
      const s = {};
      for (let i = 0; i < e.length; i++) {
        const r = e[i],
          o = `arg${i}`;
        s[o] = r;
      }
      return s;
    }
  }
  class nt {
    constructor(e, s) {
      (this.rules = e),
        (this.messages = s),
        this._compensateMessages(),
        this._sanitizeMessage();
    }
    _compensateMessages() {
      var s;
      const e = this.rules.length;
      if (this.messages.length !== e)
        for (let i = 0; i < this.rules.length; i++) {
          const r = this.convertAcoladeGroupToArray(
            (s = this.messages[i]) != null ? s : ""
          );
          for (const o of r) this.messages[o] = this.messages[i];
        }
    }
    _sanitizeMessage() {
      const e = /{(\d+(?:,\s*\d+)*)}/g;
      this.messages = this.messages.map((s) => s.replace(e, ""));
    }
    convertAcoladeGroupToArray(e) {
      var r;
      const s = /{(\d+(?:,\s*\d+)*)}/g;
      return (r = [...e.matchAll(s)].map((o) =>
        o[1].split(",").map((d) => parseInt(d.trim()))
      )[0]) != null
        ? r
        : [];
    }
    getMessages() {
      return this.messages;
    }
  }
  class at {
    constructor(e) {
      (this._nativeRules = [
        "required",
        "min",
        "max",
        "minlength",
        "maxlength",
      ]),
        (this._appliedRules = []),
        (this.inputElement = e),
        this.getNativeRulesApplied();
    }
    getNativeRulesApplied() {
      this._nativeRules.forEach((e) => {
        if (this.inputElement.hasAttribute(e)) {
          const s = this.inputElement.getAttribute(e);
          let i = e;
          s && (i = `${i}:${s}`), this._appliedRules.push(i);
        }
      });
    }
    merge(e) {
      const s = [...this._appliedRules];
      return (
        e.forEach((i) => {
          const r = s.findIndex((o) => o.startsWith(i.split(":")[0]));
          r !== -1 ? (s[r] = i) : s.push(i);
        }),
        s
      );
    }
  }
  class lt {
    constructor(e, s) {
      (this._passed = !1),
        (this.feedbackElement = null),
        (this.rules = []),
        (this.messages = {}),
        (this._errors = []),
        (this.name = ""),
        (this.showMessage = "first"),
        (this.showMessages = ["first", "full", "last"]),
        (this.validClass = ""),
        (this.invalidClass = "is-invalid"),
        (this.param = {
          emitEvent: !0,
          autoValidate: !0,
          failsOnfirst: !0,
          events: ["blur", "input", "change"],
          validClass: "",
          invalidClass: "is-invalid",
          type: "text",
        }),
        (this.validator = new ct(this.param)),
        this.setInputElement(e),
        this._setParams(s),
        this.setRules(s == null ? void 0 : s.rules),
        this.setInputName(),
        this.setFeedbackElement(),
        this.setShowMessage(),
        this._setValidationClass(),
        this._setErrors(),
        this._setEvent(s == null ? void 0 : s.events),
        this.validator.setParams(this.param);
    }
    setRules(e) {
      var r;
      let s = (r = this.inputElement.dataset.qvRules) != null ? r : "";
      if (s) {
        const o = s.split("|").filter((d) => d.length > 0);
        for (const d of o)
          if (P.hasRule(y(d).ruleName)) this.rules.push(d);
          else
            throw new Error(
              `The validation rule ${d} is not supported by QuickV`
            );
      }
      const i = new at(this.inputElement);
      return (
        (this.rules = i.merge(e != null ? e : this.rules)),
        (this.param.rules = this.rules),
        this
      );
    }
    _setEvent(e) {
      const s = this.inputElement.dataset.qvEvents;
      s &&
        (this.param.events = s.split("|").length
          ? s.split("|")
          : this.param.events),
        (this.param.events = e != null ? e : this.param.events);
    }
    setInputElement(e) {
      if (!(e instanceof Element)) {
        const s = document.querySelector(e);
        s && (e = s);
      }
      if (!(e instanceof Element))
        throw new Error(
          "The 'inputElement' parameter must be valide 'ValidatableInput' type."
        );
      (this.inputElement = e),
        (this.param.type = this.inputElement.type),
        this.inputElement.tagName.toLowerCase() === "textarea" &&
          (this.param.type = "text");
    }
    setInputName() {
      let e = this.inputElement.name;
      if (e == null || (typeof e == "string" && e.length < 0))
        throw new Error("The input name could not be empty or null");
      this.name = e;
      let s = this.name;
      this.inputElement.dataset.qvName &&
        (s = this.inputElement.dataset.qvName),
        (this.param.attribute = s);
    }
    set errors(e) {
      e && (this._errors = e != null ? e : []), this.showErrorMessages();
    }
    setFeedbackElement() {
      var r;
      let s = this.inputElement.parentElement,
        i = null;
      for (; s && !i; )
        (i = s.querySelector(`[data-qv-feedback='${this.name}']`)),
          (s = s.parentElement);
      (this.feedbackElement = i),
        (this.param.feedbackElement =
          (r = this.param.feedbackElement) != null ? r : i);
    }
    showErrorMessages() {
      if (
        (this.feedbackElement instanceof HTMLElement,
        this.feedbackElement instanceof HTMLElement)
      ) {
        let e = "";
        Array.isArray(this._errors) &&
          ((e = this._errors[0]),
          this.showMessage == "full"
            ? (e = this._errors.join("<br>"))
            : this.showMessage == "last" &&
              this._errors.length > 0 &&
              (e = this._errors[this._errors.length - 1])),
          (this.feedbackElement.innerHTML = e != null ? e : "");
      }
    }
    setShowMessage() {
      var e;
      (this.showMessage =
        (e = this.inputElement.dataset.qvShow) != null ? e : "first"),
        (this.showMessage = this.showMessages.includes(this.showMessage)
          ? this.showMessage
          : "first");
    }
    _setValidationClass() {
      var e, s, i, r;
      (this.invalidClass =
        (e = this.inputElement.dataset.qvInvalidClass) != null
          ? e
          : this.invalidClass),
        (this.validClass =
          (s = this.inputElement.dataset.qvValidClass) != null
            ? s
            : this.validClass),
        (this.invalidClass =
          (i = this.param.invalidClass) != null ? i : this.invalidClass),
        (this.validClass =
          (r = this.param.validClass) != null ? r : this.validClass);
    }
    setValidationClass() {
      const e = this._passed,
        s = (r) => {
          r.length > 0 && this.inputElement.classList.remove(r);
        },
        i = (r) => {
          r.length > 0 && this.inputElement.classList.add(r);
        };
      e
        ? (this.invalidClass.split(" ").forEach(s),
          this.validClass.split(" ").forEach(i),
          this.inputElement.setAttribute("data-qv-valid", "1"))
        : (this.validClass.split(" ").forEach(s),
          this.invalidClass.split(" ").forEach(i),
          this.inputElement.setAttribute("data-qv-valid", "0"));
    }
    _setErrors(e) {
      var r;
      const s = this.inputElement.dataset.qvMessages;
      let i = {};
      for (let o = 0; o < this.rules.length; o++) {
        let d =
          (r = s == null ? void 0 : s.split("|").map((A) => A.trim())) != null
            ? r
            : [];
        d && (d = new nt(this.rules, d).getMessages());
        const m = d !== void 0 ? d[o] : "",
          R = y(this.rules[o]).ruleName;
        typeof m == "string" && m.length > 0
          ? (i[R] = m)
          : (i[R] = T.getRuleMessage(R, T.getLocal()));
      }
      typeof e != "undefined" &&
      typeof e == "object" &&
      Object.values(e).length > 0
        ? (this.param.errors = e)
        : (this.param.errors = i);
    }
    getName() {
      return this.name;
    }
    getValue() {
      return this.inputElement.type.toLowerCase() == "file"
        ? this.inputElement.files
          ? this.inputElement.files[0]
          : null
        : this.inputElement.value;
    }
    _setParams(e) {
      typeof e == "object" &&
        typeof e != "undefined" &&
        (this.param = L(L({}, this.param), e));
    }
  }
  class ot extends lt {
    constructor(e, s) {
      super(e, s), (this._emitOnPasses = !0), (this._emitOnFails = !0);
    }
    validate(e = !0) {
      return (
        this.valid(),
        this.setValidationClass(),
        (this.errors = this.validator.getMessages()),
        e && this.emitChangeEvent(),
        this._passed
      );
    }
    getRules() {
      return this.rules;
    }
    hasRules() {
      return this.rules.length > 0;
    }
    getMessages() {
      return this.validator.getMessages();
    }
    valid() {
      return (
        (this.validator.value = this.getValue()),
        (this._passed = this.validator.passes())
      );
    }
    emit(e, s) {
      const i = new CustomEvent(e, { detail: s });
      this.inputElement.dispatchEvent(i);
    }
    on(e, s) {
      this.inputElement.addEventListener(e, s);
    }
    emitChangeEvent() {
      this.param.emitEvent &&
        (this._passed
          ? this._emitOnPasses &&
            (this.emit("qv.input.passes", {
              detail: {
                rules: this.rules,
                input: {},
                element: this.inputElement,
              },
            }),
            (this._emitOnPasses = !1),
            (this._emitOnFails = !0))
          : this._emitOnFails &&
            (this.emit("qv.input.fails", {
              detail: {
                rules: this.rules,
                input: {},
                element: this.inputElement,
              },
            }),
            (this._emitOnPasses = !0),
            (this._emitOnFails = !1)));
    }
    getErrors() {
      return this.validator.getErrors();
    }
    fails() {
      return !this.passes();
    }
    passes() {
      return this.valid();
    }
    __call(e, ...s) {
      typeof e == "function" && e(...s);
    }
  }
  class ut {
    constructor(e, s, i = null) {
      (this._value = this._trim(e)), (this.code = s), (this.attribute = i);
    }
    validateTG() {
      return /^(\+228|00228)\d{8}$/.test(this._value);
    }
    validateNE() {
      return /^(\+227|00227)\d{8}$/.test(this._value);
    }
    validateGW() {
      return /^(\+245|00245)\d{7,8}$/.test(this._value);
    }
    validateTD() {
      return /^(\+235|00235)\d{8}$/.test(this._value);
    }
    validateCM() {
      return /^(\+237|00237)\d{8}$/.test(this._value);
    }
    validateBF() {
      return /^(\+226|00226)\d{8}$/.test(this._value);
    }
    validateAO() {
      return /^(\+244|00244)[9,2][1-9]\d{7}$/.test(this._value);
    }
    validateBJ() {
      return /^(\+229|00229)\d{8}$/.test(this._value);
    }
    validateUS() {
      return /^(\+1|001)[2-9]\d{9}$/.test(this._value);
    }
    validateFR() {
      return /^(\+33|0033|0)[1-9](\d{2}){4}$/.test(this._value);
    }
    validPhoneNumber() {
      if (!(C(this._value) || $(this._value))) return !1;
      if (typeof this.code == "string") {
        const e = [],
          s = this.code.split(",").map((i) => i.trim().toUpperCase());
        for (const i of s) {
          const r = `validate${i}`,
            o = this[r];
          if (typeof o == "function") e.push(o.call(this));
          else throw new Error(`Validator method '${r}' does not exist.`);
        }
        return e.some((i) => i);
      }
      return this._validGlobally();
    }
    _validGlobally() {
      return /^(\+|00|0)[0-9]{1,3}[0-9]{1,4}[0-9]{6,13}$/.test(this._value);
    }
    _trim(e) {
      return (
        (e = typeof e != "string" ? e : String(e)),
        e.replace(/\s/g, "").replace(/-/g, "").replace(/\./g, "")
      );
    }
  }
  const ht = (t, e) => new ut(t, e).validPhoneNumber(),
    V = class {
      static rule(t, e, s, i) {
        V.addRule(t, e), V.addMessage(t, s, i);
      }
      static addRule(t, e) {
        V.rules[t] = e;
      }
      static addMessage(t, e, s) {
        T.addMessage(t, e, s);
      }
      static hasRule(t) {
        return t in V.rules;
      }
      static getRule(t) {
        return V.rules[t];
      }
      static getMessage(t, e) {
        return T.getRuleMessage(t, e);
      }
      static allRules() {
        return V.rules;
      }
      static allMessages(t) {
        return T.getMessages(t);
      }
    };
  let P = V;
  P.rules = {
    required: G,
    email: Qe,
    maxlength: se,
    minlength: te,
    min: pe,
    max: _e,
    string: C,
    between: Ve,
    startWith: Ue,
    endWith: Ge,
    contains: He,
    in: Ie,
    integer: be,
    int: be,
    modulo: we,
    number: $,
    numeric: $,
    url: ke,
    length: ie,
    len: ie,
    file: B,
    maxFileSize: H,
    minFileSize: re,
    size: xe,
    boolean: Fe,
    startWithUpper: je,
    nullable: Se,
    startWithLower: ze,
    password: Ye,
    date: ce,
    before: me,
    after: ge,
    phone: ht,
    time: Be,
    startWithLetter: Ze,
    endWithLetter: Ke,
    excludes: Xe,
    hasLetter: Je,
    regex: Pe,
    lower: tt,
    upper: et,
    stringBetween: ve,
    mod: we,
    only: We,
    mimes: st,
  };
  class ae extends ot {
    constructor(e, s) {
      super(e, s);
    }
    init() {
      var e;
      (e = this.param.events) == null ||
        e.forEach((s) => {
          this.inputElement.addEventListener(s, () => {
            this.validate();
          });
        });
    }
    rule(e, s, i) {
      P.rule(e, s, i);
    }
    with(e) {
      this._setParams(e), this.validator.setParams(this.param);
    }
    whereName(e) {
      return this.name === e;
    }
    onFails(e) {
      this.on("qv.input.fails", (s) => {
        this.__call(e);
      });
    }
    onPasses(e) {
      this.on("qv.input.passes", (s) => {
        this.__call(e);
      });
    }
    destroy() {
      var e;
      (e = this.param.events) == null ||
        e.forEach((s) => {
          this.inputElement.removeEventListener(s, () => {
            this.validate();
          });
        }),
        (this.param.events = []),
        (this.rules = []),
        (this.param.rules = []);
    }
    is(e) {
      return e === this.inputElement;
    }
  }
  class ft {
    constructor(e) {
      (this.form = e), (this.formRules = []);
    }
    getInputByName(e) {
      return this.form.querySelector(`[name=${e}]`);
    }
    getInputValueByName(e) {
      const s = this.getInputByName(e);
      return s ? s.value : void 0;
    }
    getFormRules() {
      return this.formRules;
    }
  }
  class dt extends ft {
    constructor(e) {
      super(e), this.register();
    }
    register() {
      this.formRules = [
        { ruleName: "same", call: this.same },
        { ruleName: "requiredIf", call: this.requiredIf },
        { ruleName: "requiredWhen", call: this.requiredWhen },
      ];
    }
    same(e, s) {
      return s ? e === this.getInputValueByName(s) : !1;
    }
    requiredIf(e, s) {
      const [i, ...r] = p(s != null ? s : "");
      if (i && r.length > 0) {
        const o = this.getInputValueByName(i);
        return r.includes(o) ? G(e) : !0;
      }
      return !1;
    }
    requiredWhen(e, s) {
      const [i, ...r] = p(s != null ? s : "");
      return i && r.length > 0
        ? r.some((d) => (console.log(d), G(this.getInputValueByName(d))))
          ? G(e)
          : !0
        : !1;
    }
  }
  class le {
    constructor(e, s) {
      (this._passed = !1),
        (this._emitOnPasses = !0),
        (this._emitOnFails = !0),
        (this._qvEnabledClass = "qvEnabled"),
        (this._qvDisabledClass = "qvDisabled"),
        (this._triggerValidationEvents = ["change", "qv.form.updated"]),
        (this._qvInputs = []),
        (this.config = { auto: !0 }),
        this.setContainer(e),
        (this._formValidator = new dt(this.container)),
        this.setConfig(s),
        this._initQvInputs();
    }
    setContainer(e) {
      if (!(e instanceof HTMLElement)) {
        const i = document.querySelector(e);
        i && (e = i);
      }
      if (!(e instanceof HTMLElement))
        throw new Error("The 'html container or html form' doesn't exist.");
      this.container = e;
      const s = this.container.querySelector("[data-qv-submit]");
      s && (this.submitButton = s);
    }
    init() {
      this.config.auto && (this.disableButton(), this.validateOnQvEvent()),
        this.emit("qv.form.init", this),
        this._onSubmit(),
        this.onFails((e) => {
          this.disableButton();
        }),
        this.onPasses((e) => {
          this.enableButton();
        });
    }
    disableButton() {
      var e;
      if (
        this.submitButton &&
        (this.submitButton.setAttribute("disabled", "true"),
        this._qvDisabledClass)
      ) {
        const s = this._qvEnabledClass.split(" ");
        for (const r of s) this.submitButton.classList.remove(r);
        this._qvDisabledClass =
          (e = this.submitButton.dataset.qvDisabledClass) != null
            ? e
            : "qvDisabled";
        const i = this._qvDisabledClass.split(" ");
        for (const r of i) this.submitButton.classList.add(r);
      }
    }
    enableButton() {
      var e;
      if (
        this.submitButton &&
        (this.submitButton.removeAttribute("disabled"), this._qvEnabledClass)
      ) {
        const s = this._qvDisabledClass.split(" ");
        for (const r of s) this.submitButton.classList.remove(r);
        this._qvEnabledClass =
          (e = this.submitButton.dataset.qvEnabledClass) != null
            ? e
            : "qvEnabled";
        const i = this._qvEnabledClass.split(" ");
        for (const r of i) this.submitButton.classList.add(r);
      }
    }
    validateOnQvEvent(e) {
      this.__call(e, this),
        this._triggerValidationEvents.forEach((s) => {
          this.on(s, (i) => {
            this._handle();
          });
        }),
        this._qvInputs.forEach((s) => {
          s.onFails(this._handle.bind(this)),
            s.onPasses(this._handle.bind(this));
        });
    }
    _handle() {
      (this._passed = this.isValid()),
        this._passed ? this._emitQvOnPassesEvent() : this._emitQvOnFailsEvent();
    }
    isValid() {
      return this._qvInputs.every((e) => e.passes());
    }
    passes() {
      return this.isValid();
    }
    _onSubmit() {
      this.submitButton &&
        this.submitButton.addEventListener("click", (e) => {
          if (!this._passed) {
            let s = [];
            for (const i of this._qvInputs) s.push(i.validate(!1));
            s.every((i) => i)
              ? this._emitQvOnPassesEvent()
              : (e.preventDefault(), this._emitQvOnFailsEvent());
          }
        });
    }
    rule(e, s, i) {
      P.rule(e, s, i);
    }
    setConfig(e) {
      var i, r;
      let s =
        ((i = document.querySelector("html")) == null
          ? void 0
          : i.getAttribute("data-qv-lang")) ||
        ((r = document.querySelector("html")) == null
          ? void 0
          : r.getAttribute("lang"));
      if (
        (this.container.dataset.qvLang && (s = this.container.dataset.qvLang),
        e &&
          typeof e == "object" &&
          ((this.config = L(L({}, this.config), e)), e.local))
      ) {
        const o = e.local;
        o.lang && (s = o.lang);
      }
      (T.LANG = s != null ? s : T.DEFAULT_LANG), this._syncRules();
    }
    on(e, s) {
      this.container.addEventListener(e, s);
    }
    emit(e, s) {
      const i = new CustomEvent(e, { detail: s });
      this.container.dispatchEvent(i);
    }
    onFails(e) {
      this.on("qv.form.fails", (s) => {
        this.__call(e, s.detail);
      });
    }
    onPasses(e) {
      this.on("qv.form.passes", (s) => {
        this.__call(e, s.detail);
      });
    }
    onValidate(e) {
      this.on("qv.form.validate", (s) => {
        this.__call(e, s.detail);
      });
    }
    observeChanges(e) {
      this.on("qv.form.updated", (s) => {
        this.destroyInputs(),
          this.setContainer(this.container),
          this._initQvInputs(),
          this.__call(e, this);
      });
    }
    update() {
      this.emit("qv.form.updated", this);
    }
    _initQvInputs(e) {
      (this._qvInputs = []),
        (e =
          e || Array.from(this.container.querySelectorAll("[data-qv-rules]")));
      for (const s of e) {
        const i = new ae(s, {
          validClass: this.config.validClass,
          invalidClass: this.config.invalidClass,
        });
        i.init(), this._qvInputs.push(i);
      }
    }
    __call(e, ...s) {
      typeof e == "function" && e(...s);
    }
    destroy() {
      this.container.removeEventListener("submit", this._onSubmit),
        this._triggerValidationEvents
          .filter((s) => s !== "qv.form.updated")
          .forEach((s) => {
            this.container.removeEventListener(s, this._handle);
          }),
        this.destroyInputs(),
        (this._qvInputs = []),
        this.emit("qv.form.destroy");
    }
    _emitQvOnFailsEvent() {
      this._emitOnFails &&
        (this.emit("qv.form.fails", this),
        this.emit("qv.form.validate", this),
        (this._emitOnFails = !1),
        (this._emitOnPasses = !0));
    }
    _emitQvOnPassesEvent() {
      this._emitOnPasses &&
        (this.emit("qv.form.passes", this),
        this.emit("qv.form.validate", this),
        (this._emitOnPasses = !1),
        (this._emitOnFails = !0));
    }
    _syncRules() {
      this._formValidator.getFormRules().forEach((e) => {
        this.rule(e.ruleName, e.call.bind(this._formValidator));
      });
    }
    with(e, s) {
      const i = this._qvInputs.find((r) => r.whereName(e));
      if (i) {
        const r = this._qvInputs.indexOf(i);
        i.with(s), (this._qvInputs[r] = i);
      }
    }
    withMany(e) {
      for (const [s, i] of Object.entries(e)) this.with(s, i);
    }
    onInit(e) {
      this.on("qv.form.init", (s) => {
        this.__call(e, s.detail);
      });
    }
    onUpdate(e) {
      this.on("qv.form.updated", (s) => {
        this.__call(e, s.detail);
      });
    }
    destroyInputs() {
      for (const e of this._qvInputs) e.destroy();
    }
  }
  class ye {
    constructor() {
      this.config = ne;
    }
    init(e) {
      this.setConfig(e),
        document.querySelectorAll("form").forEach((s) => {
          new le(s).init();
        });
    }
    rule(e, s, i) {
      P.rule(e, s, i);
    }
    setConfig(e) {
      (this.config = ne),
        e && typeof e == "object" && (this.config = L(L({}, this.config), e));
    }
    static Rule(e, s, i) {
      P.rule(e, s, i);
    }
  }
  class ct {
    constructor(e) {
      (this._rulesToMap = ["min", "max", "between", "size"]),
        (this._rulesFromTypes = {
          min: {
            text: "minlength",
            file: "minFileSize",
            date: "beforeDate",
            "date-local": "beforeDate",
          },
          max: {
            text: "maxlength",
            file: "maxFileSize",
            date: "afterDate",
            "date-local": "afterDate",
          },
          between: {
            text: "stringBetween",
            file: "fileBetween",
            date: "dateBetween",
            "date-local": "dateBetween",
          },
          size: { text: "length" },
        }),
        (this._inputType = "text"),
        (this._rules = []),
        (this._value = void 0),
        (this._ruleExecuted = []),
        (this._failOnfirst = !0),
        (this._attr = ""),
        (this._qvmessages = {}),
        e && this.setParams(e);
    }
    validate() {
      const e = this._rules;
      if (!Array.isArray(e))
        throw new Error("The rule provided must be an array of Rule");
      for (const s of e) {
        let { ruleName: i, params: r } = y(s),
          o = this._getRuleToRunName(i);
        const d = P.getRule(o),
          m = this._makeRuleExcutedInstance(o, i);
        if (((m.params = r), !d))
          throw new Error(`The rule ${i} is not defined`);
        if (
          (m.wasRunWith(this._value)
            ? (m.passed = m.passed)
            : ((m.passed = d(this._value, r)),
              (m.valueTested = this._value),
              (m.run = !0)),
          this._failOnfirst)
        ) {
          if (!m.passed) {
            this._parseRuleMessage(m), this._addRuleExecuted(m);
            break;
          }
        } else
          m.passed ? (m.message = "") : this._parseRuleMessage(m),
            this._addRuleExecuted(m);
      }
      return !this.hasErrors();
    }
    getErrors() {
      var s;
      const e = {};
      for (const i of this._ruleExecuted)
        i.passed || (e[i.orignalName] = (s = i.message) != null ? s : "");
      return e;
    }
    hasErrors() {
      return this._ruleExecuted.some((e) => !e.passed);
    }
    passes() {
      return !this.hasErrors();
    }
    setRules(e) {
      this._rules = e;
    }
    getRules() {
      return this._rules;
    }
    _makeRuleExcutedInstance(e, s) {
      let i = this._ruleExecuted.find((r) => r.isNamed(e));
      return i != null ? i : new mt(e, s);
    }
    _addRuleExecuted(e) {
      this._ruleExecuted.includes(e) || this._ruleExecuted.push(e);
    }
    _parseRuleMessage(e) {
      const s = T.getRuleMessage(e.orignalName),
        i = this._qvmessages[e.orignalName];
      i !== s
        ? (this._qvmessages[e.ruleName] = i)
        : (this._qvmessages[e.ruleName] = T.getRuleMessage(e.ruleName));
      const r = new rt().setMessages(this._qvmessages),
        o = r.parseMessage(
          this._attr,
          e.ruleName,
          r.getRulesMessages([e.ruleName])[0],
          e.params
        );
      return (e.message = o), e;
    }
    getMessages() {
      return this._ruleExecuted.filter((s) => !s.passed).map((s) => s.message);
    }
    set value(e) {
      (this._value = e),
        this._rules.includes("nullable")
          ? (this._value || this._value === "0") && this.validate()
          : this.validate();
    }
    get value() {
      return this._value;
    }
    setParams(e) {
      var s, i, r, o;
      (this._attr = (s = e.attribute) != null ? s : ""),
        (this._failOnfirst = e.failsOnfirst !== void 0 && e.failsOnfirst),
        (this._rules = (i = e.rules) != null ? i : []),
        (this._qvmessages = (r = e.errors) != null ? r : {}),
        (this._inputType = (o = e.type) != null ? o : this._inputType);
    }
    _getRuleToRunName(e) {
      let s = e;
      if (this._rulesToMap.includes(e)) {
        const i = this._rulesFromTypes[e];
        if (i) {
          const r = i[this._inputType];
          C(r) && r.length > 0 && (s = r);
        }
      }
      return s;
    }
  }
  class mt {
    constructor(e, s) {
      (this.passed = !1),
        (this.message = null),
        (this.run = !1),
        (this.ruleName = e),
        (this.orignalName = s);
    }
    wasRunWith(e) {
      return this.valueTested === e && this.run;
    }
    isNamed(e) {
      return this.ruleName === e;
    }
  }
  typeof window != "undefined" &&
    ((window.QvInput = (Ee = window.QvInput) != null ? Ee : ae),
    (window.QvForm = (Me = window.QvForm) != null ? Me : le),
    (window.Quickv = ($e = window.Quickv) != null ? $e : ye)),
    (M.Quickv = ye),
    (M.QvConfig = ne),
    (M.QvForm = le),
    (M.QvInput = ae),
    Object.defineProperty(M, Symbol.toStringTag, { value: "Module" });
});
