export const environment = {
  endpoints: {
    base: 'https://supportsigpcb.anatt.bj:8083/api/anatt-base',
    admin: 'https://supportsigpcb.anatt.bj:8084/api/anatt-admin',
    autoecole: 'https://supportsigpcb.anatt.bj:8080/api/anatt-autoecole',
    candidat: 'https://supportsigpcb.anatt.bj:8081/api/anatt-candidat',
    asset: 'https://supportsigpcb.anatt.bj:8082/storage/',
    fontendAdmin: 'https://supportsigpcb.anatt.bj:8050',
    recrutementexaminateur:
      'https://supportsigpcb.anatt.bj:8082/api/anatt-examinateur',
    recrutementmoniteur:
      'https://supportsigpcb.anatt.bj:8082/api/anatt-moniteur',
    entreprise: 'https://supportsigpcb.anatt.bj:8082/api/anatt-entreprise',
  },
  api_key: '_4N46y6P3cGg0#^^',
  admin: {
    asset: 'https://supportsigpcb.anatt.bj:8084/storage/',
  },
  base: {
    asset: 'https://supportsigpcb.anatt.bj:8083/storage/',
  },
  candidat: {
    asset: 'https://supportsigpcb.anatt.bj:8081/storage/',
  },
  recrutement: {
    asset: 'https://supportsigpcb.anatt.bj:8082/storage/',
  },
  entreprise: {
    base_url: 'https://supportsigpcb.anatt.bj:8082/',
    asset: 'https://supportsigpcb.anatt.bj:8082/storage/',
  },
  fedapay_key: 'pk_live_Xhh8WZkZEhIqoXatHyKXGhxi',
  recaptcha_key: '6Le8eNAoAAAAAEohMsSPbDEvfxtn8pxdDZ5IZHso',
};
