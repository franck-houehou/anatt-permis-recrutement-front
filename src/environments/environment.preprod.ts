export const environment = {
  endpoints: {
    base: 'https://formationsigpcb.anatt.bj:8083/api/anatt-base',
    admin: 'https://formationsigpcb.anatt.bj:8084/api/anatt-admin',
    autoecole: 'https://formationsigpcb.anatt.bj:8080/api/anatt-autoecole',
    candidat: 'https://formationsigpcb.anatt.bj:8081/api/anatt-candidat',
    asset: 'https://formationsigpcb.anatt.bj:8082/storage/',
    fontendAdmin: 'https://formationsigpcb.anatt.bj:8050',
    recrutementexaminateur:
      'https://formationsigpcb.anatt.bj:8082/api/anatt-examinateur',
    recrutementmoniteur:
      'https://formationsigpcb.anatt.bj:8082/api/anatt-moniteur',
    entreprise: 'https://formationsigpcb.anatt.bj:8082/api/anatt-entreprise',
  },
  api_key: '_4N46y6P3cGg0#^^',
  admin: {
    asset: 'https://formationsigpcb.anatt.bj:8084/storage/',
  },
  base: {
    asset: 'https://formationsigpcb.anatt.bj:8083/storage/',
  },
  candidat: {
    asset: 'https://formationsigpcb.anatt.bj:8081/storage/',
  },
  recrutement: {
    asset: 'https://formationsigpcb.anatt.bj:8082/storage/',
  },
  entreprise: {
    base_url: 'https://formationsigpcb.anatt.bj:8082/',
    asset: 'https://formationsigpcb.anatt.bj:8082/storage/',
  },
  fedapay_key: 'pk_live_Xhh8WZkZEhIqoXatHyKXGhxi',
  recaptcha_key: '6LckbHApAAAAADSr4TB57fvog7faq-L4wkMuRzEU',
};
